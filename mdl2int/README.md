# mdl2int

mdl2int will convert a model text into interface in typescript.

Author: gitlab.com/NithinSS

## Description

#### Model example :
   ```
   modelName {
	   property (integer, required),
	   anotherProp (Array[string], optional),
	   yetAnotherProp (string, optional) = ['Hello','world']
   }
   ```

#### Output example :
```
 export interface modelName {
  property: number ;
  anotherProp: Array<string> ;
  yetAnotherProp: YetanotherpropEnum ;
}

export enum YetanotherpropEnum {
 hello = 'Hello',
 world = 'world',
}
   ```

## Usage
  ``` 
  python mdl2int.py < input_file
  ```
   or give in as input in console (stdin), terminates on a line with '!q'(without quotes)
   In windows ==> 
   ```
   Get-Content input_file | python .\mdl2int.py
   ```

   It is recommended that you do manual verification on the automatically generated output.
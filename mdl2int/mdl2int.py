""" mdl2int(Model to Interface) will convert a model text into interface in typescript.

Author: gitlab.com/NithinSS

Description
--------------------------
Model example :
   
   modelName {
	   property (integer, required),
	   anotherProp (Array[string], optional),
	   yetAnotherProp (string, optional) = ['Hello','world']
   }

Output example :
   export interface modelName {
	 property: number ;
 	 anotherProp: Array<string> ;
 	 yetAnotherProp: YetanotherpropEnum ;
	}

   export enum YetanotherpropEnum {
	 hello = 'Hello',
 	 world = 'world',
	}

   The model doesn't create error on bad syntax and hence expects input in correct syntax.

   Usage
   ------------------------------
   python mdl2int.py < input_file // or give in as input in console terminate with a line with '!q'(without quotes)
   In windows ==> Get-Content input_file | python .\mdl2int.py

   A complete output is generated but manual verification is always recommended. Some errors might be caused on 
   pasting the output to IDEs due to the extensions like Prettier (for example maybe convert single to double quotes), 
   check their settings if Linting errors appear, the original output of this script is properly linted.
"""
import re
import sys

# config
class_name_suffix = 'Dto'
model_name_suffix = 'Model'

# add more mappings if needed
typeMap_model_ts = {
	"string" : "string",
	"integer" : "number",
	"boolean" : "boolean"
}

def typeMap_typescript(model_type):
	'''
		Handles the mapping of the model types to typescript types. Change this as required.
	'''
	if model_type.lower() in typeMap_model_ts:
		return typeMap_model_ts[model_type.lower()]
	elif model_type.startswith('Array['):
		return 'Array<'+typeMap_typescript(model_type[6:-1])+'>'
	else:
		return suffixed(model_type)

def suffixed(inp_name):
	'''
		Replaces any predefined suffix in the class name with one you want in model name, for keeping naming conventions
	'''
	if inp_name.endswith(class_name_suffix):
		return inp_name.replace(class_name_suffix, model_name_suffix) # might need more testing
	return inp_name

model_syntax = re.compile(r'(?P<name>\w+)\s*\{'				# ClassName {
							r'\s*(?P<properties>[^{]*)\s'	#  [properties]
							r'*\}')							# }

property_syntax = re.compile(r'\s*(?P<prop_name>\w+)\s*\('										# prop_name (
								r'\s*(?P<prop_type>\w+(\[\w+\]){0,1})\s*'						# 	prop_type
								r'(\,\s*(?P<prop_need>\w+)\s*){0,1}\s*\)\s*'					# , optional )
								r'(\=\s*\[(?P<enumVals>(\s*\'{0,1}\w+\'{0,1}\,{0,1})+)\]){0,1}'	# = ['Some', 'Value']
								r'\,{0,1}')														# ,

# take input until !q
input_model = []
while (True):
	l = input()
	if (l=='!q'):
		break
	else:
		input_model.append(l)
input_model = ''.join(input_model)

# take each class and create its model
models = model_syntax.finditer(input_model)
for model in models :
	props = property_syntax.finditer(model.group('properties'))
	enums_list = []

	print("export interface {name} {{".format(name = suffixed(model.group('name'))))
	for prop in props:
		sep = '?:' if (prop.group('prop_need') == 'optional') else ':' # separator 
		if(prop.groupdict()['enumVals']):
			enums_list.append((prop.group('prop_name'),prop.groupdict()['enumVals']))
			enumName = prop.group('prop_name')
			enumName = enumName[0].upper()+enumName[1:]
			print(' {name}{sep} {nameCap}Enum ;'.format(name=prop.group('prop_name'),nameCap=enumName,sep=sep))
		else:
			print(' {name}{sep} {type} ;'.format(name=prop.group('prop_name'),sep=sep, type=typeMap_typescript(prop.group('prop_type'))))
	sys.stdout.write('}\n\n')

	# default values are listed as enums
	for enums in enums_list: # enum => (enum_name, enum_values)
		vals = enums[1].split(',') # enum_values were separated by ,
		enumName = enums[0]
		print("export enum {enumNameCaps}Enum {{".format(enumNameCaps=enumName[0].upper()+enumName[1:]))
		for val in vals:
			print(' {valName} = \'{value}\','.format(valName=val.upper().strip("' "), value=val.strip(' "\'')))
		print('}\n')


	
	


